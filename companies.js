// Étape #1 : liste les entreprises ciblées, leur couleur, et leur liste de noms de domaines que doit détecter tabs.js

var companies =
[
	{
		title: "Google",
		color: "#F54B3D",
		logo: "google.svg",
		domains: 
		[
			"google.com",
			"google.fr",
			"google-analytics.com", 
			"googletagmanager.com",
			"doubleclick.net", 
			"googleadservices.com",
			"youtube.com",
			"googleapis.com",
			"googlesyndication.com",
		]
	},
	
	{
		title: "Amazon",
		color: "#FFAC30",
		logo: "amazon.svg",
		domains:
		[
			"amazon.com",
			"amazon.fr",
			"amazon-adsystem.com",
			"amazonaws.com",
			"amazonsupply.com",
		],
	},
	
	{
		title: "Facebook",
		color: "#5D80C2",
		logo: "facebook.svg",
		domains: 
		[
			"facebook.com", 
			"facebook.net",
			"facebook.fr",
			"instagram.com",
			"fbcdn.net",
		]
	},
];

