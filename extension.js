// Traite les options générales de l'extension

var extension = {};

// Indique que l'extension n'est pas prête à s'activer ni à s'afficher
extension.running = false;
extension.showing = false;
extension.report = false;

// Recupère les informations gardées en mémoire
browser.storage.local.get().then
(
	// Lorsque les informations ont bien été récupérées
	function (data) 
	{
		// Si aucun paramètre n'a encore été enregistrée (l'extension vient d'être installée)
		if (data.hasOwnProperty("running") == false)
		{
			// Initialise les paramètres
		
			data.running = true; 			// Indique si l'extension doit s'exécuter
			data.showing = true;			// Indique si l'extension doit afficher les informations sur chaque page
			data.report = false;				// Indique si l'extension signale à La Quadrature les sites détectés
			data.count_total = 0;			// Indique le nombre totale de pages affichées pendant que l'extension s'exécute
		
			// Pour chaque entreprise
			for (var i = 0; i < companies.length; i++)
			{
				data["count_"+i] = 0;	// Indique le nombre de pages d'où est partie une requête vers l'entreprise
				data["list_" + i] = [];		// Liste l'adresse de ces pages
			}
	
			// Enregistre les paramètres initalisés
			browser.storage.local.set(data);
		}
		
		// Récupère les paramètres
		extension.running = data.running; 
		extension.showing = data.showing; 
		extension.report = data.report;
	},

	// Si les paramètres n'ont pas été correctement récupérées
	function(data)
	{
		// Affiche l'erreur dans la console
		console.log("Error durring first retrieving of data:");
		console.log(data);
	}
);

// Lorsque les paramètres sont modifiés
browser.storage.onChanged.addListener(function (changes) 
{
	// Si la modification concerne l'état d'exécution de l'extension
	if (changes.hasOwnProperty("running"))
	{
		// Le met à jour
		extension.running = changes.running.newValue;
	}
	
	// Si la modification concerne le paramètre d'affichage
	if (changes.hasOwnProperty("showing"))
	{
		// Le met à jour
		extension.showing = changes.showing.newValue;
		
		// Informe tous les onglets suivis de la mise à jour
		tabs.map.forEach(	function (value, id)
		{
			browser.tabs.sendMessage( id, { type: "display", showing : extension.showing } );
		});
	}
	
});


