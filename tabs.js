// Étape #3 : transmet les informations réunies par scan.js aux onglets correspondants, où elles seront affichées par popup.js

var tabs = {};

// Liste les informations associée à chaque onglet d'où est partie une requête détectée
tabs.map = new Map(); 

/* ==== Réception des requêtes ==== /*/

// Lorsque reçoit une nouvelle requête
tabs.update = function (id, request)
{
	// Si l'onglet d'où vient la requête n'est pas encore listé
	if (this.map.has(id) == false)
	{
		// Envoie à l'onglet le script qu'il doit exécuter, ainsi que le CSS associé
		browser.tabs.executeScript		(id, {file: "popup.js", 	runAt: "document_idle"});
		browser.tabs.insertCSS			(id, {file: "popup.css", 	runAt: "document_idle"});
	
		// Ajoute l'ID de l'onglet à la liste des onglets, en lui associant un tableau qui contiendra les requêtes à conserver le temps que le script soit prêt à s'exécuter
		this.map.set(id, []);
	}
	
	// Si le script de l'onglet n'est pas encore prêt à s'exécuter
	if (this.map.get(id) != "ready")
	{
		// Enregistre la nouvelle requête dans le tableau de l'onglet, pour la lui envoyer quand il sera prêt
		this.map.get(id).push(request);
	}
	
	// Si le script de l'onglet est prêt à s'exécuter
	else
	{
		// Lui envoie la nouvelle requête
		browser.tabs.sendMessage( id, { request: request, type: "update" } );
	}
}

/* ==== Initialisation du script de l'onglet ==== /*/

// Lorsqu'un l'onglet envoie un message
browser.runtime.onMessage.addListener( function (message, tab)
{
	// Récupère l'ID de l'onglet qui a envoyé le message
	var id = tab.tab.id;
	
	// Si le message indique que le script est prêt à s'exécuter, et si l'onglet est dans la liste des onglets suivis
	if (message.type == "ready" && tabs.map.has(id))
	{
		// Transmet à l'onglet les information nécessaires à l'initialisation de son script
		browser.tabs.sendMessage( id, { type: "init", info: 
		{
			requests : tabs.map.get(id), 					// La liste des requêtes n'ayant pas encore été envoyées à l'onglet
			companies :
			{
				titles: getFromCompanies("title"), 		// La liste de noms des entreprises ciblées
				colors: getFromCompanies("color")	,	// La liste des couleurs leur étant associées
				logos: getFromCompanies("logo")		// La liste de leur logo
			},
			showing: extension.showing
		}});
		
		console.log(extension.showing);

		// Vide la liste des requêtes associées à l'onglet pour signaler qu'il est maintenant prêt à directement recevoir chaque nouvelle information
		tabs.map.set(id, "ready");
	}
	
	// Si le message indique qu'une entreprise a été repérée sur une nouvelle page
	if (message.type == "counter")
	{
		// Met à jour le compteur
		counter.update(id, message);
	}
});

/* ==== Fermeture d'une page ==== /*/

// Lorsqu'un onglet change d'état
browser.tabs.onUpdated.addListener(removeTab);
browser.tabs.onRemoved.addListener(removeTab);
function removeTab (id, info)
{
	// Si l'onglet fait parti de ceux suivi et change d'URL ou se ferme
	if (tabs.map.has(id) && info.status != "complete" && (info.hasOwnProperty('url') || info.hasOwnProperty("isWindowClosing")))
	{
		// Le retire des onglets suivis
		tabs.map.delete(id);
	}
}

/* ==== Autres ==== /*/

// Renvoie un tableau contenant un type d'information listé dans companies.js (pour être transmis au script des onglets)
function getFromCompanies(type)
{
	var array = [];
	for (var i = 0; i < companies.length; i++)
		array.push(companies[i][type]);
	return array;
}

/* ==== */

