// Mémorise et envoie le nombre et la liste des pages visitées d'où une requête a été envoyée vers une entreprise

counter = {};

// Met à jour le compteur d'une entreprise (appelé dans tabs.js en réaction à un message d'une page)
counter.update = function (id, message)
{
	// Récupère les informations enregistrées
	browser.storage.local.get().then
	(
		// Lorsque les informations ont bien été récupérées
		function (data) 
		{	
			var update = {};
	
			// Incrémente le compteur associé à l'entreprise
			update["count_"+message.index] = data["count_"+message.index] + 1;

			// Ajoute l'url de la page au début de la liste de celles enregistrées
			update["list_"+message.index] = data["list_"+message.index];
			update["list_"+message.index].unshift(message.url);
			update["list_"+message.index] = update["list_"+message.index].slice(0,500);

			// Enregistre les modifications		
			browser.storage.local.set(update);

			// Calcule la proportion
			var prop = Math.round(update["count_" + message.index] / data.count_total * 100);

			// Envoie la proporiton et la liste de 20 dernières URLs à la page à l'origine du message
			browser.tabs.sendMessage( id,
			{
				type: "counter", 
				index: message.index, 
				prop : prop, 
				list : update["list_"+message.index].slice(0,20)
			});	
		}, 
		
		// Si les informations n'ont pas été correctement récupérées
		function(data)
		{
			// Affiche l'erreur dans la console
			console.log("Error durring retrieving of counters:");
			console.log(data);
		} 
	);
}

// Lorsqu'un onglet change d'état
browser.tabs.onUpdated.addListener( function (id, info)
{
	// Si l'extension est active et que l'onglet change d'URL
	if (extension.running && info.hasOwnProperty('url') && info.status != "complete" && info.url.indexOf("about:") != 0)
	{
		// Récupère les informations enregistrées (le compteur)
		browser.storage.local.get().then( function (data) 
		{	
			// Incrémente le compteur et l'enregistre
			browser.storage.local.set({count_total : data.count_total + 1});
			console.log(info);
			console.log(data.count_total + 1);
		});
	}
});

