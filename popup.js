// Étape #4 : affiche à l'écran les informations transmises par tabs.js

	var 	popup = 	{};
	var 	timer = 	6;		// Durée d'affichage en seconde (infini si 0)

// Signale à tabs.js que la page est chargée ======================
browser.runtime.sendMessage({type:"ready"});


// Réception des messages envoyés par tabs.js ===================
browser.runtime.onMessage.addListener( function (message)
{
	// S'il s'agit du message d'initialisation
	if (message.type == "init")
	{
		// Récupère les noms et couleurs d'entreprises
		popup.titles = message.info.companies.titles;
		popup.colors = message.info.companies.colors;
		popup.logos = message.info.companies.logos;
		popup.showing = message.info.showing;
		
		console.log(message);

		// Pour chaque requête de la liste transmise par tabs.js
		for (var request of message.info.requests)
		{	
			// Met à jour les informations sur la requête
			popup.update(request);
		}
	}
			
	// S'il s'agit d'un message de mise à jour des requêtes
	if (message.type == "update")
	{
		// Met à jour les informations sur la requête
		popup.update(message.request);
	}
	
	// S'il s'agit d'un message de mise à jour du compteur
	if (message.type == "counter")
	{
		// L'élément général qui inclue le compteur
		var 	counter = document.createElement("div");
				counter.className = "counter";
		popup.companies.get(message.index).info.appendChild(counter);
		
		// Le début du texte
		var 	counter1 = document.createElement("div");
				counter1.innerHTML = browser.i18n.getMessage("counter1");
				counter.appendChild(counter1);
		
		// Le pourcentage
		var 	counter2 = document.createElement("div");
				counter2.className = "prop";
				counter2.style.color = popup.colors[message.index];
				counter2.innerHTML = "<b style='background: " + popup.colors[message.index] +";'>" + message.prop + "%</b> "+ browser.i18n.getMessage("counter2");
				counter.appendChild(counter2);

		// La liste des requêtes à afficher		
		var urlList = "";
		for (var u of message.list)
			urlList += "<li>"+u+"</li>";
			
		// L'infobulle 
		var 	counterDiv = document.createElement("div");
				counterDiv.className = "counter_div";
				counterDiv.style.backgroundColor = popup.colors[message.index];
				counterDiv.style.color = popup.colors[message.index];
				counterDiv.innerHTML = browser.i18n.getMessage("counterList", [popup.companies.get(message.index).title, urlList]);
				counter.appendChild(counterDiv);
		
		// La fin du texte
		var 	counter2 = document.createElement("div");
				counter2.innerHTML = browser.i18n.getMessage("counter3", popup.companies.get(message.index).title);
				counter.appendChild(counter2);
				
		// Outro
		var 	outro = document.createElement("div");
				outro.innerHTML = browser.i18n.getMessage("outro");
				outro.style.color = popup.colors[message.index];
				outro.className = "outro";
				outro.onclick = function() {window.open("https://laquadrature.net", '_blank');}
				popup.companies.get(message.index).info.appendChild(outro);
				
	}
	
	// S'il s'agit d'un message concernant l'état de l'affichage
	if (message.type == "display")
	{
		popup.showing = message.showing;
		popup.companies.forEach(function (comp)
		{
			if (popup.showing)
				popup.startDisplay(comp.div);
			else
			{
				comp.div.className = "company";
				clearTimeout(comp.div.timeoutID);
			}
		});
	}
});


// Mise à jour =========================================
popup.update = function (request)
{

	// Si aucune entreprise n'est encore listée =====================
	if (this.hasOwnProperty("companies") == 0)
	{
		// Créé la liste des entreprises à suivre
		popup.companies = new Map(); 
		
		// Créé l'élément HTML de plus haut niveau
		this.div = document.createElement("div");		
		this.div.id = "gafam_detector";
		document.body.appendChild(this.div);
	}

	// Si l'entreprise concernée par la requête n'est pas encore listée ======
	if (this.companies.has(request.index) == false)
	{
		// Créé l'objet contenant les informations sur l'entreprise
		var company =
		{
			index:		request.index,
			title: 		this.titles[request.index],	// Nom de l'entreprise
			color:		this.colors[request.index],	// Couleur de l'entreprise
			logo:		this.logos[request.index],
			contents: new Map(),							// Liste des ressources proposées
		};
		
		// Ajoute l'objet au tableau des entreprises suivies 
		this.companies.set(request.index, company);

		// Créé l'élément HTML qui contiendra toutes les informations sur l'entreprise
		company.div = document.createElement("div");
		company.div.className = "company";
		company.div.style.backgroundColor = company.color;
		company.div.style.top = 15 + 110 * (this.companies.size -1 ) + "px";
		this.div.appendChild(company.div);
		
		// Le logo de l'entreprise
		var 	title = document.createElement("div");
				title.className = "title";
				title.innerHTML = "<img src='" + browser.extension.getURL(company.logo) +"'>" ;
				// title.style.color = company.color;
				company.div.appendChild(title);
			
		// Le sous-texte du drapeau
		var subtitle = document.createElement("div");
				subtitle.className = "subtitle";
				subtitle.innerHTML = browser.i18n.getMessage("subtitle");
		company.div.appendChild(subtitle);
	
		// Les détails
		company.info = document.createElement("div");
		company.info.className = "info";
		company.div.appendChild(company.info);
		
		// Premier paragraphe des détails (nom du site + nom de l'entreprise)
		var 	intro = document.createElement("div");
				intro.className = "intro";
				intro.style.color = company.color;
				intro.innerHTML = browser.i18n.getMessage("intro", [window.location.href.replace(/.*?\:\/\/(?:www.)?(.*?)\/.*/, "$1"), company.title]);
		company.info.appendChild(intro);
			
		// Liste des ressources offertes par l'entreprise
		company.list = document.createElement("div");
		company.list.className = "type_list";
		company.list.style.color = company.color;
		company.info.appendChild(company.list);
		
		// Si la page a le focus
		if (!document.hidden)
		{	
			// Lance l'affichage de l'élément HTML
			this.startDisplay(company.div);
		}
	
		// Si la page n'a pas le focus
		else
		{
			// Appelle le lancement de drapeau dès qu'elle prend le focus
			document.addEventListener('visibilitychange', function(e) {if (!document.hidden) popup.startDisplay(company.div );});
		}
		
		// Informe l'extension qu'une nouvelle page est connue par l'entreprise
		browser.runtime.sendMessage({type:"counter", index: company.index, url: window.location.href});
	}
	
	// Si le type de ressource n'est pas encore listé ==================
	if (this.companies.get(request.index).contents.has(request.content) == false)
	{
		// Créer l'objet contenant les informations sur le type de ressource
		var content =
		{
			type : request.content,	// Type de ressource
			number : 0,						// Nombre de fois où elle a été proposée
		}
		
		// Créé l'élément HTML qui affichera le nombre de fois où le type de ressource a été offert
		content.div = document.createElement("div");
		content.div.className = "type";
		content.div.style.color = this.colors[request.index];
		this.companies.get(request.index).list.appendChild(content.div);
				
		// Créé l'élément HTML qui listera les requêtes concernant le type de ressource
		var 	div = document.createElement("div");
				div.className = "type_requests";
				div.style.backgroundColor = this.colors[request.index];
		this.companies.get(request.index).list.appendChild(div);
		
		// Créé l'élément HTML qui introduit la liste
		var 	intro = document.createElement("div");
				intro.className = "intro";
				intro.innerHTML = browser.i18n.getMessage("request", [request.content, new Date(request.date).toLocaleString().slice(0, -3), window.location.href]);
		div.appendChild(intro);

		// Créé l'élément HTML qui listera les URL des requêtes
		content.list = document.createElement("ol");
		content.list.className = "type_url";
		div.appendChild(content.list);
		
		// Ajoute l'objet à la liste des types suivis
		this.companies.get(request.index).contents.set(request.content, content);
	}

	// Met à jour les informations concernant le type de ressource offert ====
	var content = this.companies.get(request.index).contents.get(request.content);
	
	// Incrémente le compteur de la ressource
	content.number ++;
	
	// Met à jour l'élément HTML affichant le nombre de requêtes concernant une ressource
	content.div.innerHTML = "<b style='color: " + this.colors[request.index] + ";'>" + content.number + "</b>&nbsp;" + content.type + (content.number > 1 ? "s" : "");
	content.div.style.background = this.colors[request.index];
	
	// Ajoute l'URL de la requête à la liste de celles associées à un type de ressource
	var 	limit = 100;
	var 	url = request.url.length < limit ? request.url : request.url.substr(0, limit - 4) + " [&hellip;]";
			url = url.replace(/\:\/\/(.*?\.)?([^\.]*?\.[^\.]*?)\//, "://$1<b><u>$2</u></b>/");
	var 	newRequ = document.createElement("li");
			newRequ.innerHTML = url;
	content.list.appendChild(newRequ);
}


// Affichage du drapeau ==================================
popup.startDisplay = function(div)
{
	if (popup.showing)
	{
		// Attend 10ms pour afficher le drapeau
		setTimeout
		(
			function(d){d.className += " show";}, 
			10, 
			div
		);

		// Attend la durée indiquée par popup.timer pour fermer le drapeau
		if (timer)
		{
			div.timeoutID = setTimeout
			(
				function(d){d.className += " fold";}, 
				1000 * timer,
				div
			);
		}
	}
}

returnedValue = true;
