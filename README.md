# Gafam Detector

## Description
Module Firefox destiné à rendre visible les requêtes HTTP faites par les GAFAM sur presque tout le web, leur permettant de nous y tracer partout.

Ce qu'il fait déjà (au 26 févr. 2018)
* Affiche le nombre de requête en fonction du type de ressource demandée (image, script, css...) et liste les URLs des ressources
* Affiche la proportion des pages où chaque Gafam a été rencontré par rapport au total des pages visitées (et liste les adresses des pages)
* Peut désactiver la détection de Gafam ou l'affichage via le menu de l'extension
* Ne signale pas les Gafam sur leurs propres sites
* Est prêt à être traduit (il suffit de copier le dossier fr dans _locales en d'autres langues)

À faire :
* Permettre d'envoyer les sites détectés à LQDN afin qu'on puisse faire un top 5 des sites à attaquer au 25 mai
* Collecter plus de noms de domaines associés aux Gafam
* Améliorer l'aspect du module
* Traduire

## Installation
1. Télécharger le zip (icône de petit nuage sur le GitLab, ici)
2. Le décompresser
2. Dans Firefox, entrer dans la barre d'URL : about:debugging 
3. Cliquer sur Charger un module complémentaire
4. Choisissez manifest.json depuis le zip décompressé
5. Désactivez vos adblockers pour plus de fun !

## Licence 
GPL3 sauce yolo

## Aperçu

<img src="preview.png">



