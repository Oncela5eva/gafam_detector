// détecte les requêtes HTTP faites par les nom de domaines listés dans domains.js et le envoie à tabs.js

// Lorsque le navigateur fait une requête HTTP (depuis n'importe quelle URL et à partir de n'importe quel onglet),
// exécute la fonction d'analyse en lui passant en paramètre les détails de la requête et de son header
browser.webRequest.onBeforeSendHeaders.addListener(scan , {urls: ["<all_urls>"] } , ["requestHeaders"] );

// Analyse une requête
function scan(request) 
{
	// Si l'extention est active
	if (extension.running)
	{
		// Recherche le referer et l'host de la requête
		var referer = 0;
		var host = 0;
		for (var header of request.requestHeaders)
		{
			if (header.name == "referer")
				referer = header.value;
			else if (header.name == "host")
				host = header.value;
		}
	
		// Si la requête a été faite depuis un onglet et si elle a un referer et un host
		if (request.tabId != -1 && referer && host)
		{
			// Commence la comparaison
			var keepScanning = true;
			
			// Pour chaque nom de domaine de chaque entreprise
			for (var i = 0; i < companies.length && keepScanning == true; i++)
			{
				for (var j = 0; j < companies[i].domains.length && keepScanning == true; j++)
				{
					// Si l'host de la requête contient le nom de domaine
					if (host.indexOf(companies[i].domains[j]) != -1)
					{
						// Arrête l'analyse si le referer contient un des noms de domaine associés à l'entreprise
						for (var domain of companies[i].domains)
						{
							if (referer.indexOf(domain) != -1)
							{
								var keepScanning = false;
								break;
							}
						}	
						// Prépare les information à afficher à l'utilisateur
						var info = {};
						info.index	= i;												// Index de l'entreprise dont le nom de domaine correspond à celui de l'host la requête 
						info.title 			= 	companies[i].title;			// Nom de l'entreprise
						info.content 	= request.type; 					// Type de ressource demandée par la requête (image, script, css...)
						info.url				=	request.url;						// URL complète de la ressource demandée par la requête
						info.date 			= request.timeStamp; 		// Date de la requête

						// Passe les informations à tabs.js
						tabs.update(request.tabId, info);
										
						// Arrête l'analyse
						keepScanning = false;
						break;
					}
				}
			}
		}
	}
}

