browser.storage.local.get().then( function (data)
// onload = function()
{
		// Titre
		var 	title = document.createElement("h1");
				title.innerHTML = browser.i18n.getMessage("title");
				document.body.appendChild(title);
			
		var 	options = document.createElement("div");
				options.className = "options";
				document.body.appendChild(options);
		
		var	activate = newSlider(options, "Activer", data.running, true);
				activate.title = "Active l'extension";
				activate.onclick = function() 
				{
					this.toggle();
					this.className = "slider " + (this.open ? "open" : "close");
					
					if (this.open)
					{
						show.open = true;
						show.clickable = true;
						show.className = "slider open" ;
					}
					else
					{
						show.clickable = false;
						show.open = false;
						show.className = "slider close unactive" ;
					}
					browser.storage.local.set(	{running : this.open, showing : this.open} );
				}
		
		var	show = newSlider(options, "Afficher", data.showing, data.running);
				activate.title = "Affiche un drapeau visible lorsque repère un GAFAM";
				show.onclick = function() 
				{
					if (this.clickable)
					{
						this.toggle();
						this.className = "slider " + (this.open ? "open" : "close");
						browser.storage.local.set(	{showing : this.open} );
					}
				}
				
		// var	reduce = newSlider(options, "Réduire", data.showing, data.running);
		
		/*
		var	report = newSlider(options, "Signaler", data.report, data.running);
				report.onclick = function() 
				{
					if (!data.report)
					{
						var choice = confirm
						(
								"\nClique sur « OK » si tu souhaites envoyer automatiquement à La Quadrature du Net la liste des sites où tu détecteras un GAFAM.\n"
							+ "\nCeci nous permettra de lister les sites détectés par les personnes utilisant le GAFAM Detector.\n"
							+ "\nNous n'aurons pas accès à l'adresse précise des pages que tu visites et, évidemment, nous ne concerverons pas l'adresse IP depuis laquelle tu nous enverras ces signalements ni n'essaierons d'individualiser ceux-ci d'aucune façon..\n\nNous n'utiliserons les données reçues que pour lister les sites détectés.\n"
							+ "\nTu pourras arrêter de nous envoyer tes signalements en recliquant sur le bouton qui a ouvert cette fenêtre.\n"
							+ "\nMerci pour ton aide !"
						);
					
						if (choice)
						{
							this.toggle();
							this.className = "slider open";
							browser.storage.local.set(	{report : true} );
						}
					}
					else
					{
						this.toggle();
						this.className = "slider close";
						browser.storage.local.set(	{report : false} );
					}
				}
			*/
				
				
		// Outro
		var 	outro = document.createElement("div");
				outro.innerHTML = "Plus d'informations sur<br><b>laquadrature.net</b>";
				outro.className = "outro";
				document.body.appendChild(outro);
				outro.onclick = function() {browser.tabs.create({"url": "https://laquadrature.net"}); close();}
});

function newSlider (parent, title, state, otherState) 
{
	var 	div = document.createElement("div");
			div.innerHTML = title;
			parent.appendChild(div);
			div.open = state;
			div.clickable = otherState ;
			
			div.className = "slider " + (state && otherState ? "open" : "close") + (div.clickable ? "" : " unactive");
			
			div.toggle = function ()
			{
				if (this.open)
					this.open = false;
				else
					this.open = true;
			}
			
	return div;
}

